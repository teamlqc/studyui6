package com.example.studyui6;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener {

    private static final int REQUEST_CODE_PHOTO = 101;
    public static final String KEY_PHOTO = "KEY_PHOTO";


    private ImageView mIvPhoto;
    private Bitmap mbitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();

    }

    private void initViews() {
        findViewById(R.id.tv_capture).setOnClickListener(this);
        mIvPhoto = findViewById(R.id.iv_photo);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.tv_capture:
                capturePhoto();
                break;
            default:
                break;

        }

    }

    private void capturePhoto() {
        Intent intent = new Intent();
        intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,REQUEST_CODE_PHOTO);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case REQUEST_CODE_PHOTO:
                getPhoto(resultCode,data);
                break;
            default:
                break;
        }
    }

    private void getPhoto(int resultCode, Intent data) {
        if(resultCode == RESULT_CANCELED){
            Toast.makeText(this, "Cannot get photo", Toast.LENGTH_SHORT).show();
        }else if(data!=null){
            Bundle bundle = data.getExtras();
            mbitmap = (Bitmap) bundle.get("data");

            mIvPhoto.setImageBitmap(mbitmap);
            Intent intent = new Intent();
            intent.putExtra(KEY_PHOTO,mbitmap);
            setResult(RESULT_OK,intent);

        }
    }




}
