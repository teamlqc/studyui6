package com.example.studyui6;

import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class Act3 extends Activity implements View.OnClickListener {
    private static final int LEVEL_CHECK = 1;
    private static final int LEVEL_UN_CHECK = 0;
    private ImageView mIvCheck;
    private LevelListDrawable mLvDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_act3);
        initViews();
    }

    private void initViews() {
        mIvCheck = findViewById(R.id.iv_check);
        mIvCheck.setOnClickListener(this);

        //cach2
        mLvDrawable = (LevelListDrawable) mIvCheck.getDrawable();


        ImageView ivBattery = findViewById(R.id.iv_battery);
        AnimationDrawable animationDrawable = (AnimationDrawable) ivBattery.getDrawable();
        animationDrawable.start();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_check:
                //cach1
                int level = mIvCheck.getDrawable().getLevel();
//                mIvCheck.setImageLevel(level==LEVEL_CHECK?LEVEL_UN_CHECK:LEVEL_CHECK);


                mLvDrawable.setLevel(level==LEVEL_CHECK?LEVEL_UN_CHECK:LEVEL_CHECK);

                break;
            default:
                break;
        }
    }
}
