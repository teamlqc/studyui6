package com.example.studyui6;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;

import com.example.studyui6.dialog.ActionDialog;

public class SuccessDialog extends Dialog implements View.OnClickListener {

    private ActionDialog mCallBack;

    public SuccessDialog(Context context) {
        super(context,R.style.AppThemeDialog);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_dialog);
        initViews();
    }

    private void initViews() {
        findViewById(R.id.tv_ok).setOnClickListener(this);
    }

    public void setmCallBack(ActionDialog mCallBack) {
        this.mCallBack = mCallBack;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_ok:
//                showToast();

                CommonUtil.getInstance().
                        showAlertDialog(getContext(), "Alert", "This is Alert",
                                "OK", null, null, mCallBack);//ko xu ly gi ca thi action truyen null
                break;
            default:
                break;
        }
    }
}
