package com.example.studyui6;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class HomeActivity extends Activity implements View.OnClickListener {

    private static final int REQUEST_CODE_TAKE_PHOTO = 105;
    private ImageView mIvPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();

    }

    private void initViews() {
        TextView tvCapture = findViewById(R.id.tv_capture);
        tvCapture.setText("Request Phto");
        tvCapture.setOnClickListener(this);
        mIvPhoto = findViewById(R.id.iv_photo);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.tv_capture:
                capturePhoto();
                break;
            default:
                break;

        }

    }

    private void capturePhoto() {
        Intent intent = new Intent();
        intent.setClass(this,MainActivity.class);
//        intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,REQUEST_CODE_TAKE_PHOTO);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case REQUEST_CODE_TAKE_PHOTO:
                getPhoto(resultCode,data);
                break;
            default:
                break;
        }
    }

    private void getPhoto(int resultCode, Intent data) {
        if(resultCode == RESULT_CANCELED){
            Toast.makeText(this, "Cannot get photo", Toast.LENGTH_SHORT).show();
        }else if(data!=null){
            Bitmap bitmap = data.getParcelableExtra(MainActivity.KEY_PHOTO);

            mIvPhoto.setImageBitmap(bitmap);
        }
    }
}
