package com.example.studyui6;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.example.studyui6.dialog.ActionDialog;
import com.example.studyui6.dialog.MyToast;

public class Act4 extends Activity implements View.OnClickListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_act3);
        initViews();
    }

    private void initViews() {
        findViewById(R.id.iv_battery).setOnClickListener(this);

    }


    private void showToast() {
        MyToast myToast = new MyToast(this);
//        myToast.initToast("Hello World", Gravity.CENTER).show();
//        myToast.show();
        myToast.initToast("Hello World").show();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_battery:
                SuccessDialog successDialog = new SuccessDialog(this);
                //truyen vao 1 callback xu ly ok
                successDialog.setmCallBack(new ActionDialog() {
                    @Override
                    public void handleBt1() {
                        CommonUtil.getInstance().notifySms(Act4.this,"Hello");
                    }
                });
                successDialog.show();
                break;
            default:
                break;
        }
    }
}
