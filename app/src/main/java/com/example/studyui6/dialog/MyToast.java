package com.example.studyui6.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.example.studyui6.R;

public class MyToast {
    private Toast myToast;
    private Context mContext;

    public MyToast(Context mContext) {
        this.mContext = mContext;
    }

    public MyToast initToast(String text, int gravity){
        myToast = Toast.makeText(mContext,text,Toast.LENGTH_LONG);
        myToast.setGravity(Gravity.CENTER,50,0);
        return this;
    }

    public MyToast initToast(String text){

        //lay chieu rong man hinh
        int wScreen = mContext.getResources().getDisplayMetrics().widthPixels;
        //chieu dai
        int hScreen = mContext.getResources().getDisplayMetrics().heightPixels;

        //null la khong dua vao 1 giao dien cu the nao ca,chi nhung vao thoi
        View view = View.inflate(mContext,R.layout.item_face,null);
        myToast = new Toast(mContext);
        myToast.setView(view);
        myToast.setGravity(Gravity.CENTER,0,0);
        return this;
    }

    public void show(){
        if(myToast==null)return;
        myToast.show();
    }



}
