package com.example.studyui6;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

import com.example.studyui6.dialog.ActionDialog;
import com.example.studyui6.dialog.MyToast;

public class CommonUtil {

    private static CommonUtil INSTANCE;

    private CommonUtil(){
        //for singleton pattern design
        //nguoi dung o the tao thuc the moi
    }

    public static CommonUtil getInstance(){
        if(INSTANCE == null){
            INSTANCE = new CommonUtil();
        }
        return INSTANCE;
    }

    public void showAlertDialog(final Context context, String title, String content,
                                String bt1, String bt2, String bt3,
                                final ActionDialog action
                                ){
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(content);
        //KHANG DINH: POSITIVE
        if(bt1!=null){
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
                    "Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(action!=null){
                                action.handleBt1();
                            }
                        }
                    });
        }
        if(bt2!=null){
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,
                "No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(action!=null){
                            action.handleBt2();
                        }
                    }
                });}
        if(bt3!=null){
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL,
                "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(action!=null){
                            action.handleBt3();
                        }
                    }
                });}
        alertDialog.show();
    }

    public void notifySms(Context context,String str) {
        Toast.makeText(context, ""+str, Toast.LENGTH_SHORT).show();
    }


}
